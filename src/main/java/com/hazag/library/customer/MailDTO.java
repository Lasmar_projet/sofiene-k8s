package com.hazag.library.customer;

import io.swagger.annotations.ApiModel;

@ApiModel (value = "Mail Model")
public class MailDTO {

    public final String Mail_FORM = "elasmer.sofiene@gmail.com";

    private Integer customerId ;
    private String emailSubject ;
    private String emailContent ;

    public String getMail_FORM() {
        return Mail_FORM;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getEmailContent() {
        return emailContent;
    }

    public void setEmailContent(String emailContent) {
        this.emailContent = emailContent;
    }
}

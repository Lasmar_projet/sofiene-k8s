package com.hazag.library.customer;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.Date;
import java.util.Optional;

@Service("customer")
public class CustomerServiceImp implements CustomerService {

    public static final Logger LOGGER = LoggerFactory.getLogger(CustomerRestController.class);

    @Autowired
    private  CustomerRepository customerRepository;

    @Autowired
    private JavaMailSender javaMailSender ;

    @Override
    public Customer checkIsExist(Integer id) {
        Optional<Customer> customer = customerRepository.findById(id);
        if(customer.isPresent())
         return customer.get();
        return null ;
    }

    @Override
    public Customer saveCustomer(Customer customer) {
        customer.setCreationDate(LocalDate.now());
        return customerRepository.save(customer);
    }

    public Customer findCustomerById(Integer id){
         Optional<Customer> customer = customerRepository.findById(id);
         if(customer.isPresent()) return customer.get();
         return null ;
    }

    public ResponseEntity<Boolean> sendMailToCustomerService(MailDTO loanMailDTO){
        Customer customer = findCustomerById(loanMailDTO.getCustomerId());
        if(customer == null){
            String errormessage = "The selected Customer for sending email is not found in the database ";
            LOGGER.info(errormessage);
            return new ResponseEntity<Boolean>(false , HttpStatus.NOT_FOUND);
        }else if (customer != null && StringUtils.isEmpty(customer.getEmail())){
            String errorMessage = "No Existing email for the selected Customer for sending email to";
            LOGGER.info(errorMessage);
            return  new ResponseEntity<>(Boolean.FALSE , HttpStatus.NOT_MODIFIED);
        }
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(loanMailDTO.Mail_FORM);
        simpleMailMessage.setTo(customer.getEmail());
        simpleMailMessage.setSentDate(new Date());
        simpleMailMessage.setSubject(loanMailDTO.getEmailSubject());
        simpleMailMessage.setText(loanMailDTO.getEmailContent());
        try {
            javaMailSender.send(simpleMailMessage);
        }catch (MailException e){
            return new ResponseEntity<Boolean>(false  , HttpStatus.OK);
        }
        return new ResponseEntity<Boolean>(true , HttpStatus.OK);
    }
}

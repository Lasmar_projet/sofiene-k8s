package com.hazag.library.book;

import com.hazag.library.category.Category;
import com.hazag.library.loan.Loan;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Table
@Entity(name = "BOOK")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Book implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BOOK_ID")
    private Integer id ;

    @Column(name = "TITLE" )
    private String title ;
    @Column(name = "ISBN", nullable = false , unique = true)
    private String isbn ;
    @Column(name = "RELASE_DATE")
    private LocalDate relaseDate ;
    @Column(name = "REGISTER_DATE" )
    private LocalDate registerDate;
    @Column(name = "TOTAL_EXEMPLAIRES")
    private Integer totalExemplaireS;
    @Column(name = "AUTHOR")
    private String author ;
    @ManyToOne(optional = false)
    @JoinColumn(name = "GAT_CODE" , referencedColumnName = "CODE")
    private Category category ;
    @OneToMany(fetch=FetchType.LAZY , mappedBy = "pk.book" , cascade = CascadeType.ALL)
    private Set<Loan> loans ;

    public Book(Integer id , String isbn){
        this.id = id; this.isbn = isbn ;
    };

}

package com.hazag.library.book;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface BookRepository extends JpaRepository<Book , Integer> , CrudRepository<Book , Integer> {

    Optional<Book> findByIsbn(String isbn);

    @Query("SELECT bk FROM BOOK bk inner join bk.category cat where cat.code = :code")
    List<Book> getBooksByCategory(@Param("code") String code);

}

package com.hazag.library.book;

import java.util.List;

public interface BookService {

    Book checkExisting(String isbn) ;
    Book saveBook(Book book);
    boolean checkIdExist(Integer id);
    void deleteBook(Integer bookid);
    List<Book> getBooksByCategory(String codeCategory);

}

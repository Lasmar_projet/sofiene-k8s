package com.hazag.library.book;

import com.hazag.library.category.Category;
import com.hazag.library.category.CategoryDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.parser.Entity;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/rest/book/api")
@Api(value = "Book Rest Controller test : contains all operation for managing books")
public class BookRestController {

    @Autowired
     private ModelMapper modelMapper;

    @Autowired
    private BookServiceImpl bookServiceImp ;
    @Autowired
    private KafkaTemplate<String , String> stringKafkaTemplate ;

    @GetMapping("/send/{message}/{topic}")
    public String send(@PathVariable String message , @PathVariable String topic) {

        stringKafkaTemplate.send(topic , message);

        return "Message sent ......";
    }
    @PostMapping("/addBook")
    @ApiOperation(value = "Add new Book in the Library")
    @ApiResponses(value = {
            @ApiResponse(code = 409 ,message = "Conflit : the book already exist"),
            @ApiResponse(code = 201 , message = "Created : the book is successfully inserted")
    })
    public ResponseEntity<BookDTO> createNewBook(@RequestBody BookDTO bookDTORequest){

        Book bookexisting = bookServiceImp.checkExisting(bookDTORequest.getIsbn());

        if(bookexisting != null){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        Book bookRequest = mapBookDTOTobook(bookDTORequest);
        Book book = bookServiceImp.saveBook(bookRequest);
        BookDTO bookDTO = mapBookToBookDTO(book);
        return new ResponseEntity<BookDTO>(bookDTO , HttpStatus.CREATED)  ;
    }

    @PutMapping("/updateBook")
    @ApiOperation(value = "Update Book existing in library", response = BookDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 404 , message = "NOT FOUND : the book does not exist"),
            @ApiResponse(code = 200 , message = "OK : the book is successfully update"),
            @ApiResponse(code = 304 , message = "NOT MODIFIED : this book is unsuccessfully update ")
    })
    public ResponseEntity<BookDTO> updateBook(@RequestBody BookDTO bookDTO){

        if(!bookServiceImp.checkIdExist(bookDTO.getId())){
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
            Book bookRequest = mapBookDTOTobook(bookDTO);
            Book book = bookServiceImp.saveBook(bookRequest);
            if (book != null){
                return new ResponseEntity<BookDTO>(bookDTO , HttpStatus.OK);
            }
           return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);

    }

    @DeleteMapping("/deleteBook/{bookId}")
    @ApiOperation(value = "Delete book in the library" , response = String.class)
    @ApiResponse(code = 204 , message = "No Content : Book sucessefully  delete")
    public ResponseEntity<String> deleteBook(@PathVariable Integer bookId){

           bookServiceImp.deleteBook(bookId) ;
           return  new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/getBooksByCategorie/{codeCategory}")
    @ApiOperation(value = "get list book by Code Category" , response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200 , message = "OK : successfull research"),
            @ApiResponse(code = 204 , message = "No Content : nos resultat found")
    })
    public ResponseEntity<List<BookDTO>> searchBooksByCategory(@PathVariable String codeCategory ){

            List<Book> books = bookServiceImp.getBooksByCategory(codeCategory);
            if(!CollectionUtils.isEmpty(books)){
                List<BookDTO> bookDTOS = books.stream().map(book -> {
                    return mapBookToBookDTO(book);
                }).collect(Collectors.toList());
                return new ResponseEntity<List<BookDTO>>(bookDTOS ,HttpStatus.OK);
            }
            return  new ResponseEntity<>(HttpStatus.NO_CONTENT) ;
    }
        private Book  mapBookDTOTobook(BookDTO bookDTO){
           Book book =  modelMapper.map(bookDTO , Book.class);
           book.setCategory(new Category(bookDTO.getCategoryDTO().getCode() , ""));
           book.setRegisterDate(LocalDate.now());
           return book ;
    }
        private BookDTO mapBookToBookDTO(Book book){
           BookDTO bookDTO = modelMapper.map(book, BookDTO.class );
           bookDTO.setCategoryDTO(new CategoryDTO(book.getCategory().getCode()));
           return bookDTO ;
        }
}

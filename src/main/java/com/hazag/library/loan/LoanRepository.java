package com.hazag.library.loan;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface LoanRepository extends JpaRepository<Loan , Long > , CrudRepository<Loan , Long> {


    @Query("SELECT lo FROM Loan lo inner join lo.pk.book bk inner join lo.pk.customer cust WHERE bk.id = ?1 and cust.id = ?2 and lo.status = ?3")
    Optional<Loan> getLoanByLoanTest(Integer bookid ,Integer customerid ,LoanStatus status);


    List<Loan> findByEndDateAndStatus(LocalDate localDate , LoanStatus loanStatus);
}

package com.hazag.library.loan;

import com.hazag.library.book.Book;
import com.hazag.library.customer.Customer;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.time.LocalDateTime;

@Embeddable
 class LoanId implements Serializable {

    private static final long serialVersionUID = 3912193101593832821L;

    @ManyToOne
    private Book book ;

    @ManyToOne
    private Customer customer ;



    public LoanId() {
        super();
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

}
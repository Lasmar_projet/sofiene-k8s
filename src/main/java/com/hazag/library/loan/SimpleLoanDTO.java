package com.hazag.library.loan;

import com.hazag.library.book.BookDTO;
import com.hazag.library.customer.CustomerDTO;

import java.time.LocalDate;

public class SimpleLoanDTO {

    private Integer customerId ;

    private Integer bookId ;

    private LocalDate loanbeginDate;

    private LocalDate loanendDate;

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public LocalDate getLoanbeginDate() {
        return loanbeginDate;
    }

    public void setLoanbeginDate(LocalDate loanbeginDate) {
        this.loanbeginDate = loanbeginDate;
    }

    public LocalDate getLoanendDate() {
        return loanendDate;
    }

    public void setLoanendDate(LocalDate loanendDate) {
        this.loanendDate = loanendDate;
    }
}

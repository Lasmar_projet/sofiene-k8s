package com.hazag.library.loan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class LoanServiceImpl implements LoanService {

    @Autowired
    private LoanRepository loanRepository ;

    @Override
    public Loan checkIfExist(SimpleLoanDTO simpleLoanDTO) {
    Optional<Loan> loan =  loanRepository.getLoanByLoanTest(simpleLoanDTO.getBookId() , simpleLoanDTO.getCustomerId() , LoanStatus.OPEN) ;
  return loan.get() ;
    }

    @Override
    public Loan saveLoan(Loan loan) {
           Loan loanSave = loanRepository.save(loan);
        return  loanSave ;
    }

    @Override
    public List<Loan> findLoansByStatusIsOpenAndDateBeforeDateJour(LocalDate localDate, LoanStatus loanStatus) {
        loanRepository.findByEndDateAndStatus(localDate , loanStatus);
        return loanRepository.findByEndDateAndStatus(localDate , loanStatus);
    }
}

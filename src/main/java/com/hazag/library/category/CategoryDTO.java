package com.hazag.library.category;

public class CategoryDTO {

    private String code ;

    private String label;

    public CategoryDTO() {
    }

    public CategoryDTO(String code){
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
